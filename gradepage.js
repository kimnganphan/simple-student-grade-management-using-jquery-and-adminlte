$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  //Tạo biến chứa thứ tự các cột trong bảng
  const gGRADE_COLUMNS = [
    "stt",
    "studentId",
    "subjectId",
    "grade",
    "examDate",
    "action",
  ];
  //Thứ tự các cột
  const gSTT_COL = 0;
  const gSTUDENT_COL = 1;
  const gSUBJECT_COL = 2;
  const gGRADE_COL = 3;
  const gEXAM_DATE_COL = 4;
  const gACTION_COL = 5;

  //Tạo biến chứa số thứ tự
  var gSTT = 1;
  //Tạo biến chứa id
  var gId = [];
  //tạo biến chứa student id
  var gStudentId = [];
  //tạo biến chứa subject id
  var gSubjectId = [];
  /** Các biến toàn cục hằng số Form mode: 4 trạng thái của form. Mặc định sẽ là Normal
   *
   ** Khi ấn vào nút Thêm, cần đổi biến trạng thái về trạng thái Insert
   ** Khi ấn vào nút Sửa, cần đổi biến trạng thái về trạng thái Update
   ** Khi ấn vào nút Xóa, cần đổi biến trạng thái về trạng thái Delete
   *
   * Tại một thời điểm, trạng thái của form luôn là 1 trong 4 trạng thái
   */
  var gFORM_MODE_NORMAL = "Normal";
  var gFORM_MODE_INSERT = "Insert";
  var gFORM_MODE_UPDATE = "Update";
  var gFORM_MODE_DELETE = "Delete";

  // biến toàn cục cho trạng thái của form: mặc định ban đầu là trạng thái Normal
  var gFormMode = gFORM_MODE_NORMAL;

  /**
   * Khi ấn vào nút thêm thông tin học sinh, trạng thái sẽ là Student
   * Khi ấn vào nút thêm thông tin môn học, trạng thái sẽ là Subject
   * Khi ấn vào nút thêm thông tin điểm và ngày thi, trạng thái sẽ là Grade
   * * Tại một thời điểm, trạng thái của modal luôn là 1 trong 4 trạng thái
   */
  var gMODAL_NORMAL = "none";

  var gMODAL_MODE_STUDENT = "Student";
  var gMODAL_MODE_SUBJECT = "Subject";
  var gMODAL_MODE_GRADE = "Grade";
  //Biến toàn cục cho trạng thái của modal: mặc định ban đầu khi chưa chọn là None
  var gModalMode = gMODAL_NORMAL;
  //TẠO biến chứa bảng (thư viện datatable)
  var gGradeTable = $("#student-table").DataTable({
    ordering: false, // không sắp xếp theo thứ tự
    columns: [
      { data: gGRADE_COLUMNS[gSTT_COL] }, //cột số thứ tự
      { data: gGRADE_COLUMNS[gSTUDENT_COL] }, // cột tên học sinh
      { data: gGRADE_COLUMNS[gSUBJECT_COL] }, // cột môn học
      { data: gGRADE_COLUMNS[gGRADE_COL] }, // cột điểm
      { data: gGRADE_COLUMNS[gEXAM_DATE_COL] }, // cột ngày thi
      { data: gGRADE_COLUMNS[gACTION_COL] }, // cột chứa nút edit và delete
    ],
    columnDefs: [
      {
        //định nghĩa lại cột stt
        targets: gSTT_COL,
        render: function () {
          return gSTT++;
        },
      },
      {
        //định nghĩa lại cột tên học sinh
        targets: gSTUDENT_COL,
        render: function (data) {
          //render lại cột tên để lấy đầy đủ cả họ và tên học sinh
          //data: gGrades.studentId

          for (var bI = 0; bI < gStudentDB.students.length; bI++) {
            //nếu data (gGrades.studentId) = với id trong gStudentDB.students thì trả về firstname và lastname của student
            if (data === gStudentDB.students[bI].id) {
              return (
                gStudentDB.students[bI].firstname +
                " " +
                gStudentDB.students[bI].lastname
              );
            }
          }
        },
      },
      {
        //định nghĩa lại cột môn học
        targets: gSUBJECT_COL,
        render: function (data) {
          //data: gGrades.grades.subjectId
          //nếu data (gGrades.grades.subjectId) = với id trong gSubjects.subjects thì trả về tên môn học (subjectName)
          for (var bI = 0; bI < gSubjects.subjects.length; bI++) {
            if (data === gSubjects.subjects[bI].id) {
              return `${gSubjects.subjects[bI].subjectName}`;
            }
          }
        },
      },
      {
        //định nghĩa lại cột action
        //2 nút: edit và delete
        targets: gACTION_COL,
        defaultContent: `
                <button class="btn text-white btn-sm edit-grade" style="background-color:#B38540"><i class="fas fa-pencil-alt mr-1" data-toggle="tooltip" data-placement="bottom" title="Edit"></i>Edit</button>
                <button class="btn text-white btn-sm delete-grade" style="background-color:#B6452C"><i class="fas fa-trash-alt mr-1" data-toggle="tooltip" data-placement="bottom" title="Delete"></i>Delete</button>

                `,
      },
    ],
  });
  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  //thực thi sự kiện load trang
  onPageLoading();

  //Gán sự kiện cho nút lọc dữ liệu
  $("#btn-filter-data").on("click", onBtnFilterDataClick);

  
  //gán sự kiện cho nút thêm mới dữ liệu
  $("#btn-add-data").on("click", function () {
    onBtnAddNewData();
  });
  //gán sự kiện cho nút edit
  $("#student-table").on("click", ".edit-grade", function () {
    onBtnEditClick(this);
  });

  //gán sự kiện cho nút delete
  $("#student-table").on("click", ".delete-grade", function () {
    onBtnDeleteClick(this);
  });
  //gán sự kiện cho nút xác nhận delete
  $("#btn-confirm-delete").on("click", onBtnDeleteConfirmClick);
  //gán sự kiện cho nút xác nhận thêm hoặc sửa thông tin
  $("#btn-confirm").on("click", onBtnConfirmClick);
  //gán sự kiện cho nút thêm thoong tin học sinh
  $("#btn-add-info").on("click", function () {
    //hiện thông tin học sinh
    $("#div-show-info").show();
    //ẩn 3 nút (thêm thông tin học sinh, thêm thông tin môn học, thêm thông tin điểm và ngày thi)
    $("#div-button").hide();
    //set chể độ của modal thành Student
    gModalMode = gMODAL_MODE_STUDENT;
    $("#div-mod").html(gModalMode);
  });

  //gán sự kiện cho nút quay lại trang ban đầu trong trang nhập thông tin học sinh
  $("#btn-come-back-main").on("click", function () {
    //ẩn vùng thông tin học sinh
    $("#div-show-info").hide();
    //hiện 3 nút (thêm thông tin học sinh, thêm thông tin môn học, thêm thông tin điểm và ngày thi)
    $("#div-button").show();
    //set chế độ của modal thành None
    gModalMode = gMODAL_NORMAL;
    $("#div-mod").html(gModalMode);
  });

  //gán sự kiện cho nút thêm thoong tin môn học
  $("#btn-add-subject").on("click", function () {
    //hiển thị vùng thông tin môn học
    $("#div-show-subject").show();
    //ẩn 3 nút (thêm thông tin học sinh, thêm thông tin môn học, thêm thông tin điểm và ngày thi)
    $("#div-button").hide();
    //set chế độ của modal thành Subject
    gModalMode = gMODAL_MODE_SUBJECT;
    $("#div-mod").html(gModalMode);
  });
  //gán sự kiện cho nút quay lại trang ban đầu trong trang nhập thông tin môn học
  $("#btn-come-back-main2").on("click", function () {
    //ẩn vùng thông tin môn học
    $("#div-show-subject").hide();
    //hiện 3 nút (thêm thông tin học sinh, thêm thông tin môn học, thêm thông tin điểm và ngày thi)
    $("#div-button").show();
    //set chế độ của modal thành None
    gModalMode = gMODAL_NORMAL;
    $("#div-mod").html(gModalMode);
  });

  //gán sự kiện cho nút thêm thoong tin điểm
  $("#btn-add-grade").on("click", function () {
    //hiển thị vùng thông tin điểm và ngày thi
    $("#div-show-grade").show();
    //ẩn 3 nút (thêm thông tin học sinh, thêm thông tin môn học, thêm thông tin điểm và ngày thi)
    $("#div-button").hide();
    //set chế độ của modal thành Grade
    gModalMode = gMODAL_MODE_GRADE;
    $("#div-mod").html(gModalMode);
  });
  //gán sự kiện cho nút quay lại trang ban đầu trong trang nhập thông tin điểm
  $("#btn-come-back-main3").on("click", function () {
    //ẩn vùng thông tin điểm và ngày thi
    $("#div-show-grade").hide();
    //hiển thị 3 nút (thêm thông tin học sinh, thêm thông tin môn học, thêm thông tin điểm và ngày thi)
    $("#div-button").show();
    //set chế độ của modal thành None
    gModalMode = gMODAL_NORMAL;
    $("#div-mod").html(gModalMode);
  });
  //reset dữ liệu khi click nút cancel
  $("#student-modal").on("hidden.bs.modal", function () {
    //set lại chế độ của modal thành None
    gModalMode = gMODAL_NORMAL;
    $("#div-mod").html(gModalMode);
    //set lại chế độ của form thành normal
    gFormMode = gFORM_MODE_NORMAL;
    $("#div-form-mod").html(gFormMode);
    //reset lại các trường dữ liệu trong modal
    resetForm();
  });
  //reset dữ liệu khi click nút cancel DELETE MODAL
  $("#delete-modal").on("hidden.bs.modal", function () {
    //set lại chế độ của form thành normal
    gFormMode = gFORM_MODE_NORMAL;
    $("#div-form-mod").html(gFormMode);
    //reset lại các trường dữ liệu trong modal
    resetForm();
  });
  //gán sự kiện cho nút check username  có bị trùng hay không
  $("#btn-check-username").on("click", function () {
    onBtnUsernameCheckClick();
  });
  //gán sự kiện cho nút check student code  có bị trùng hay không
  $("#btn-check-studentcode").on("click", function () {
    onBtnStudentCodeCheckClick();
  });
  //gán sự kiện cho nút check email có bị trùng hay không
  $("#btn-check-email").on("click", function () {
    onBtnCheckEmailClick();
  });

  //gán sự kiện cho nút check subject code có bị trùng hay không
  $("#btn-check-subjectcode").on("click", function () {
    onBtnCheckSubjectCodeClick();
  });
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  //hàm xử lý sự kiện load trang
  function onPageLoading() {
    "use strict";
    //khi load trang trạng thái ban đầu là normal
    gFormMode = gFORM_MODE_NORMAL;
    $("#div-form-mod").html(gFormMode);
    //khi load trang trạng thái của modal là none
    gModalMode = gMODAL_NORMAL;
    $("#div-mod").html(gModalMode);
    //trích xuất id select student trong filter
    var vSelectStudentName = $("#select-student");
    //trích xuất id select student trong modal (trong phần thông tin điểm và ngày thi)
    var vSelectStudentNameModal = $("#select-student-modal");
    //trích xuất id select môn học trong filter
    var vSelectSubject = $("#select-subject");
    //trích xuất id select môn học trong modal (trong phần thông tin điểm và ngày thi)
    var vSelectSubjectModal = $("#select-subject-modal");
    //Load thông tin học sinh vào bảng (datatable)
    loadStudentGradeToTable(gGrades.grades);
    //load tên học sinh vào select student trong filter
    loadStudentNameToSelect(vSelectStudentName);
    //load tên học sinh vào select student trong modal (trong phần thông tin điểm và ngày thi)
    loadStudentNameToSelect(vSelectStudentNameModal);
    //load môn học vào select trên filter
    loadSubjectNameToSelect(vSelectSubject);
    //load môn học vào select môn học modal (trong phần thông tin điểm và ngày thi)
    loadSubjectNameToSelect(vSelectSubjectModal);
  }

  //Hàm xử lý sự kiện ấn nút lọc dữ liệu
  function onBtnFilterDataClick() {
    "use strict";
    //Tạo đối tượng cần lọc
    var vFilterData = {
      studentId: 0, // lấy student id
      subjectId: 0, //lấy subject id
    };

    //Thu thập dữ liệu
    vFilterData.studentId = $("#select-student").val(); // value của select student
    vFilterData.subjectId = $("#select-subject").val(); // value của select tên môn học
    //console log ra dữ liệu đã được thu thập
    console.log(vFilterData);
    //gọi phương thức filter trong đối tượng grade, tạo biến chứa kết quả lọc
    var vFilterResult = gGrades.filter(vFilterData);
    //console log ra kết quả lọc để theo dõi
    console.log(vFilterResult);
    //Hiển thị kết quả lọc lên bảng
    loadStudentGradeToTable(vFilterResult);
  }
  //Hàm xử lý sự kiện ấn nút thêm mới dữ liệu
  function onBtnAddNewData() {
    "use strict";
    //Set trang thái của form insert
    gFormMode = gFORM_MODE_INSERT;
    $("#div-form-mod").html(gFormMode);
    //Hiển thị modal
    $("#student-modal").modal("show");
    if (gModalMode === gMODAL_NORMAL) {
      // nếu modal ở trạng thái none
      $("#div-show-grade").hide(); // ẩn vùng thông tin điểm ngày thi
      $("#div-show-info").hide(); // ấn vùng thông tin học sinh
      $("#div-show-subject").hide(); // ẩn vùng thông tin môn học
    }
  }

  //Hàm xử lý sự kiện ấn nút edit
  function onBtnEditClick(paramEditButton) {
    "use strict";
    //Set trang thái update
    gFormMode = gFORM_MODE_UPDATE;
    $("#div-form-mod").html(gFormMode);
    //Hiển thị modal
    $("#student-modal").modal("show");
    //lấy dữ liệu của dòng
    var vRowSelected = $(paramEditButton).parents("tr");
    var vDatatableRow = gGradeTable.row(vRowSelected);
    var vRowData = vDatatableRow.data();
    //lấy id của dòng
    gId = vRowData.id;
    console.log("id: " + gId);
    //lấy gstudentid
    gStudentId = vRowData.studentId;
    console.log("student id: " + gStudentId);
    //lấy subject id
    gSubjectId = vRowData.subjectId;
    console.log("gSubject id:" + gSubjectId);
    //tHÊM read only vào các trường input không muốn sửa
    addReadOnlyToInput();
    //disabled các nút check
    disableCheckButton();
    //Hiển thị thông tin học sinh vào bảng
    loadStudentInfoToModal(vRowData);
    //khi chưa chọn thông tin muốn hiển thị lên modal:
    if (gModalMode === gMODAL_NORMAL) {
      // nếu modal ở trạng thái none
      $("#div-show-grade").hide(); // ẩn vùng thông tin điểm ngày thi
      $("#div-show-info").hide(); // ấn vùng thông tin học sinh
      $("#div-show-subject").hide(); // ẩn vùng thông tin môn học
    }
  }

  //Hàm xử lý sự kiện ấn nút xác nhận thêm mới hoặc sửa thông tin trong modal
  function onBtnConfirmClick() {
    "use strict";
    //nếu modal ở trạng thái thêm hoặc sửa thông tin học sinh (Student)
    if (gModalMode === gMODAL_MODE_STUDENT) {
      //tạo đối tượng student
      var vStudentObj = {
        id: 0,
        studentCode: "",
        username: "",
        firstname: "",
        lastname: "",
        birthday: "",
        email: "",
        studyYear: 0,
      };
      //Thu thập dữ liệu các đối tượng
      getStudentObjInfo(vStudentObj);
      //console log để theo dõi đối tượng đã thu thập dữ liệu
      console.log(vStudentObj);
      //kiếm tra xem các thông tin có hợp lệ hay không
      var vIsValidated = validateStudentObject(vStudentObj);
      if (vIsValidated) {
        // nếu thông tin hợp lệ thì thực hiện các việc sau:
        //lưu thông tin học sinh vào gStudentDB (thêm mới hoặc sửa)
        saveStudentData(vStudentObj);
        //thông báo khi cập nhật dữ liệu thành công
        alert("Cập nhật dữ liệu thành công");
        //tắt modal sau khi thêm hoặc sửa thành công
        $("#student-modal").modal("hide");
        //reset trạng thái ban đầu của form là normal
        gFormMode = gFORM_MODE_NORMAL;
        $("#div-form-mod").html(gFormMode);
        //reset trạng thái ban đầu của modal là none
        gModalMode = gMODAL_NORMAL;
        $("#div-mod").html(gModalMode);
        //reset lại các trường dữ liệu trong modal
        resetForm();
        //load lại dữ liệu vào select student name trong modal (thông tin điểm và ngày thi) và filter
        var vSelectStudentName = $("#select-student");
        var vSelectStudentNameModal = $("#select-student-modal");
        $("#select-student").empty();
        $("#select-student-modal").empty();
        loadStudentNameToSelect(vSelectStudentName);
        loadStudentNameToSelect(vSelectStudentNameModal);
        //load lại thông tin vào bảng sau khi thêm hoặc sửa
        loadStudentGradeToTableAfterUpdate(gGrades.grades);
      }
    }
    //nếu modal ở chế độ thêm hoặc sửa thông tin môn học (Subject)
    if (gModalMode === gMODAL_MODE_SUBJECT) {
      //tạo đối tượng môn học
      var vSubjectObj = {
        id: 0,
        subjectCode: "",
        subjectName: "",
        credit: 0,
      };
      //thu thập dữ liệu thông tin môn học
      getSubjectObjInfo(vSubjectObj);
      // kiểm tra xem các trường input có hợp lệ hay chưa
      var vIsValidated = validateSubject(vSubjectObj);
      if (vIsValidated) {
        // nếu tất cả các trường dữ liệu đã hợp lệ:
        //Lưu các thông tin môn học đã thêm hoặc sửa vào gSubjectDB
        saveSubjectData(vSubjectObj);
        //thông báo khi cập nhật dữ liệu thành công
        alert("Cập nhật dữ liệu thành công");
        //tắt modal sau khi thêm thành công
        $("#student-modal").modal("hide");
        //reset dữ liệu trong modal
        resetForm();
        //reset trạng thái form ban đầu là normal
        gFormMode = gFORM_MODE_NORMAL;
        $("#div-form-mod").html(gFormMode);
        //reset trạng thái của modal ban đầu là none
        gModalMode = gMODAL_NORMAL;
        $("#div-mod").html(gModalMode);
        var vSelectSubject = $("#select-subject");
        var vSelectSubjectModal = $("#select-subject-modal");
        //Load dữ liệu mới vào select môn học trong filter và modal (thông tin điểm và ngày thi)
        $("#select-subject").empty();
        $("#select-subject-modal").empty();
        //load môn học vào select trong filter
        loadSubjectNameToSelect(vSelectSubject);
        //load môn học vào select modal (thông tin điểm và ngày thi)
        loadSubjectNameToSelect(vSelectSubjectModal);
        //load lại dữ liệu lên bảng sau khi thêm hoặc sửa
        loadStudentGradeToTableAfterUpdate(gGrades.grades);
      }
    }
    //nếu modal ở chế độ thêm hoặc sửa thông tin điểm và ngày thi (Grade)
    if (gModalMode === gMODAL_MODE_GRADE) {
      //tạo đối tượng điểm và ngày thi
      var vGradeObj = {
        id: 0,
        studentId: 0,
        subjectId: 0,
        grade: 0,
        examDate: "",
      };
      //thu thập dữ liệu
      getGradeObjInfo(vGradeObj);
      // kiểm tra xem các trường input có hợp lệ hay chưa
      var vIsValidated = validateGrade(vGradeObj);
      if (vIsValidated) {
        // nếu tất cả các trường dữ liệu đã hợp lệ:
        //Lưu các thông tin điểm và ngày thi đã thêm hoặc sửa vào gGrade
        saveGradeData(vGradeObj);

        //thông báo khi cập nhật dữ liệu thành công
        alert("Cập nhật dữ liệu thành công");
        //tắt modal sau khi thêm thành công
        $("#student-modal").modal("hide");
        //reset dữ liệu trong modal
        resetForm();
        //reset trạng thái form ban đầu là normal
        gFormMode = gFORM_MODE_NORMAL;
        $("#div-form-mod").html(gFormMode);
        //reset trạng thái của modal là none
        gModalMode = gMODAL_NORMAL;
        $("#div-mod").html(gModalMode);
        //load lại dữ liệu lên bảng sau khi thêm hoặc sửa
        loadStudentGradeToTableAfterUpdate(gGrades.grades);
      }
    }
  }
  //hàm xử lý sự kiện ấn nút kiểm tra username (thông tin học sinh)
  function onBtnUsernameCheckClick() {
    "use strict";
    //trích xuất id của modal xác nhận
    var vConfirmModal = $("#confirm-modal");
    //lấy thông tin trường dữ liệu username
    var vInpUsername = $("#inp-username").val().trim();
    if (vInpUsername === "") {
      //nếu không nhập gì
      //hiển thị modal xác nhận
      vConfirmModal.modal("show");
      //nội dung xác nhận trong modal
      $("#confirm-content").html("Bạn chưa nhập username");
    }
    if (isExistUsername(vInpUsername)) {
      //nếu username nhập vào đã tồn tại
      //hiển thị modal xác nhận
      vConfirmModal.modal("show");
      //nội dung xác nhận trong modal
      $("#confirm-content").html("Username đã tồn tại");
    }
    if (!isExistUsername(vInpUsername) && vInpUsername != "") {
      //nếu có nhập username và username đó không trùng
      //hiển thị modal xác nhận
      vConfirmModal.modal("show");
      //nội dung xác nhận trong modal
      $("#confirm-content").html("Username hợp lệ");
    }
  }

  //Hàm xử lý sự kiện ấn nút kiểm tra student code (thông tin học sinh)
  function onBtnStudentCodeCheckClick() {
    "use strict";
    //trích xuất id của modal xác nhận
    var vConfirmModal = $("#confirm-modal");
    //lấy thông tin trường dữ liệu student code
    var vInpStudentCode = $("#inp-studentcode").val().trim();
    if (vInpStudentCode == 0) {
      // nếu không nhập student code
      //hiển thị modal xác nhận
      vConfirmModal.modal("show");
      //nội dung xác nhận trong modal
      $("#confirm-content").html("Bạn chưa nhập student code");
    }
    if (isExistStudentCode(vInpStudentCode)) {
      //nếu student code bị trùng
      //hiển thị modal xác nhận
      vConfirmModal.modal("show");
      //nội dung xác nhận trong modal
      $("#confirm-content").html("Student code đã tồn tại");
    }
    if (!isExistStudentCode(vInpStudentCode) && vInpStudentCode != "") {
      //nếu có nhập student code và studentcode đó không trùng
      //hiển thị modal xác nhận
      vConfirmModal.modal("show");
      //nội dung xác nhận trong modal
      $("#confirm-content").html("Student code hợp lệ");
    }
  }

  //Hàm xử lý sự kiện ấn nút kiểm tra email
  function onBtnCheckEmailClick() {
    "use strict";
    //trích xuất id của modal xác nhận
    var vConfirmModal = $("#confirm-modal");
    //lấy thông tin trường dữ liệu email
    var vInpEmail = $("#inp-email").val().trim();
    if (vInpEmail == "") {
      // nếu không nhập email
      //hiển thị modal xác nhận
      vConfirmModal.modal("show");
      //nội dung xác nhận trong modal
      $("#confirm-content").html("Bạn chưa nhập email");
    }
    if (isExistEmail(vInpEmail)) {
      // nếu email bị trùng
      //hiển thị modal xác nhận
      vConfirmModal.modal("show");
      //nội dung xác nhận trong modal
      $("#confirm-content").html("Email đã tồn tại");
    }
    if (!validateEmail(vInpEmail)) {
      //nếu email đã nhập có định dạng ko chính xác
      //hiển thị modal xác nhận
      vConfirmModal.modal("show");
      //nội dung xác nhận trong modal
      $("#confirm-content").html(
        "Định dạng email không chính xác (a@gmail.com)"
      );
    }
    if (
      !isExistEmail(vInpEmail) &&
      vInpEmail != "" &&
      validateEmail(vInpEmail)
    ) {
      //nếu email có nhập, ko trùng và định dạng hợp lệ
      //hiển thị modal xác nhận
      vConfirmModal.modal("show");
      //nội dung xác nhận trong modal
      $("#confirm-content").html("Email hợp lệ");
    }
  }
  //Hàm xử lý sự kiện ấn nút kiểm tra subject code (thông tin môn học)
  function onBtnCheckSubjectCodeClick() {
    "use strict";
    //trích xuất id của modal xác nhận
    var vConfirmModal = $("#confirm-modal");
    //lấy thông tin trường dữ liệu subject code
    var vInpSubjectCode = $("#inp-subjectcode").val().trim();

    if (vInpSubjectCode === "") {
      //nếu ko nhập subject code
      //hiển thị modal xác nhận
      vConfirmModal.modal("show");
      //nội dung xác nhận trong modal
      $("#confirm-content").html("Bạn chưa nhập subject code");
    }
    if (isExistSubjectCode(vInpSubjectCode)) {
      //nếu subject code bị trùng
      //hiển thị modal xác nhận
      vConfirmModal.modal("show");
      //nội dung xác nhận trong modal
      $("#confirm-content").html("Subject code đã tồn tại");
    }
    if (!isExistSubjectCode(vInpSubjectCode) && vInpSubjectCode != "") {
      //nếu subject code k bị trùng
      //hiển thị modal xác nhận
      vConfirmModal.modal("show");
      //nội dung xác nhận trong modal
      $("#confirm-content").html("Subject code hợp lệ");
    }
  }
  //Hàm xử lý sự kiện ấn nút delete
  function onBtnDeleteClick(paramDeleteButton) {
    "use strict";
    //Set trang thái của form Delete
    gFormMode = gFORM_MODE_DELETE;
    $("#div-form-mod").html(gFormMode);
    //Hiển thị modal xác nhận xoá
    $("#delete-modal").modal("show");
    //lấy dữ liệu của dòng
    var vRowSelected = $(paramDeleteButton).parents("tr");
    var vDatatableRow = gGradeTable.row(vRowSelected);
    var vRowData = vDatatableRow.data();

    //lấy id của dòng
    gId = vRowData.id;
    console.log(gId);
  }
  //Hàm xử lý sự kiện ấn nút xác nhận xoá
  function onBtnDeleteConfirmClick() {
    "use strict";
    //Gọi phương thức xoá trong gGrade
    gGrades.deleteGrade(gId);
    //Hiển thị lại dữ liệu lên bảng sau khi xoá
    loadStudentGradeToTableAfterUpdate(gGrades.grades);
    //Thông báo cập nhật dữ liệu thành công
    alert("Cập nhật dữ liệu thành công");
    //Tắt modal
    $("#delete-modal").modal("hide");
    //Reset trang thái form thành normal
    gFormMode = gFORM_MODE_NORMAL;
    $("#div-form-mod").html(gFormMode);
    //reset dữ liệu trong modal
    resetForm();
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //Hàm xử lý sự kiện load thông tin học sinh và điểm vào bảng
  function loadStudentGradeToTable(paramStudentArr) {
    "use strict";
    gSTT = 1;
    gGradeTable.clear(); // clear bảng
    gGradeTable.rows.add(paramStudentArr); // thêm dữ liệu vào các dòng
    gGradeTable.draw(); // tạo bảng
  }
  //Hàm xử lý sự kiện load thông tin học sinh và điểm vào bảng sau khi update
  function loadStudentGradeToTableAfterUpdate(paramStudentArr) {
    "use strict";
    gSTT = 1;
    gGradeTable.clear(); // clear bảng
    gGradeTable.rows.add(paramStudentArr); // thêm dữ liệu vào các dòng
    gGradeTable.draw(false); // tạo bảng (ko trở lại trang 1 mà sẽ load lại bảng tại chỗ thêm sửa xoá)
  }
  //Hàm xử lý sự kiện load tên học sinh vào select student (trong filter và modal(thông tin điểm và ngày thi))
  //inp: paramId: id của select
  //out: load dữ liệu tên học sinh vào các select được chọn
  function loadStudentNameToSelect(paramId) {
    "use strict";
    //thêm option chọn học sinh
    paramId.append(
      $("<option>", {
        value: "",
        text: "Chọn học sinh",
      })
    );
    //thêm option tên các học sinh
    for (var bI = 0; bI < gStudentDB.students.length; bI++) {
      paramId.append(
        $("<option>", {
          value: gStudentDB.students[bI].id,
          text: `${gStudentDB.students[bI].firstname}  ${gStudentDB.students[bI].lastname}`,
        })
      );
    }
  }

  //Hàm xử lý sự kiện load môn học vào select subject (trong filter và modal(thông tin điểm và ngày thi))
  //inp: paramId: id của select
  //out: load dữ liệu môn học vào các select được chọn
  function loadSubjectNameToSelect(paramId) {
    "use strict";
    //thêm option chọn môn học
    paramId.append(
      $("<option>", {
        value: "",
        text: "Chọn môn học",
      })
    );
    //thêm option tên các môn học
    for (var bI = 0; bI < gSubjects.subjects.length; bI++) {
      paramId.append(
        $("<option>", {
          value: gSubjects.subjects[bI].id,
          text: gSubjects.subjects[bI].subjectName,
        })
      );
    }
  }

  //Hàm xử lý sự kiện load thông tin lên modal (chức năng edit thông tin)
  //inp: paramStudentInfo: thông tin muốn hiển thị lên bảng
  //out: hiển thị thông tin của paramStudentInfo lên bảng
  function loadStudentInfoToModal(paramStudentInfo) {
    "use strict";
    //Hiển thị phần thêm thông tin điểm thi và ngày thi
    $("#select-student-modal").val(paramStudentInfo.studentId);
    $("#select-subject-modal").val(paramStudentInfo.subjectId);
    $("#inp-diemthi").val(paramStudentInfo.grade);
    $("#inp-ngaythi").val(paramStudentInfo.examDate);

    //Hiển thị thông tin môn học
    for (var bI = 0; bI < gSubjects.subjects.length; bI++) {
      if (paramStudentInfo.subjectId == gSubjects.subjects[bI].id) {
        $("#inp-subjectname").val(gSubjects.subjects[bI].subjectName);
        $("#inp-subjectcode").val(gSubjects.subjects[bI].subjectCode);
        $("#inp-credit").val(gSubjects.subjects[bI].credit);
      }
    }

    //Hiển thị thông tin cá nhân học sinh
    for (var bI = 0; bI < gStudentDB.students.length; bI++) {
      if (paramStudentInfo.studentId == gStudentDB.students[bI].id) {
        $("#inp-username").val(gStudentDB.students[bI].username);
        $("#inp-studentcode").val(gStudentDB.students[bI].studentCode);
        $("#inp-fname").val(gStudentDB.students[bI].firstname);
        $("#inp-lname").val(gStudentDB.students[bI].lastname);
        $("#inp-studyyear").val(gStudentDB.students[bI].studyYear);
        $("#inp-bday").val(gStudentDB.students[bI].birthday);
        $("#inp-email").val(gStudentDB.students[bI].email);
      }
    }
  }

  //Thêm read only vào các trường dữ liệu khi không muốn dữ liệu được sửa mà chỉ được xem
  function addReadOnlyToInput() {
    "use strict";
    //Thông tin học sinh
    $("#inp-username").prop("readonly", true);
  }
  //hàm xử lý xét id
  function getStudentNextId() {
    "use strict";
    var vNextId = 0;
    // nếu mảng chưa có phần tử nào, thì id sẽ bắt đầu từ 1
    if (gStudentDB.students.length == 0) {
      vNextId = 1;
    } else {
      // id tiếp theo bằng id của phần tử cuối cùng cộng thêm 1
      vNextId = gStudentDB.students[gStudentDB.students.length - 1].id + 1;
    }
    return vNextId;
  }
  //Hàm xử lý thu thập dữ liệu
  function getStudentObjInfo(paramStudentObj) {
    "use strict";
    if (gFormMode === gFORM_MODE_INSERT) {
      //thêm mới
      paramStudentObj.id = getStudentNextId();
    } else {
      //sửa
      paramStudentObj.id = gStudentId;
    }

    paramStudentObj.username = $("#inp-username").val().trim();
    paramStudentObj.studentCode = $("#inp-studentcode").val().trim();
    paramStudentObj.firstname = $("#inp-fname").val().trim();
    paramStudentObj.lastname = $("#inp-lname").val().trim();
    paramStudentObj.studyYear = $("#inp-studyyear").val().trim();
    paramStudentObj.birthday = $("#inp-bday").val().trim();
    paramStudentObj.email = $("#inp-email").val().trim();
  }
  //Validate dữ liệu học sinh xem có hợp lệ hay không
  function validateStudentObject(paramStudentObj) {
    "use strict";
    //validate username
    if (paramStudentObj.username === "") {
      alert("Xin vui lòng nhập username");
      return false;
    }
    if (isExistUsername(paramStudentObj.username)) {
      alert("Username bị trùng");
      return false;
    }
    //validate student code
    if (paramStudentObj.studentCode == 0) {
      alert("Xin vui lòng nhập student codes");
      return false;
    }
    if (isExistStudentCode(paramStudentObj.studentCode)) {
      alert("Student code bị trùng");
      return false;
    }
    //validate first name
    if (paramStudentObj.firstname === "") {
      alert("Xin vui lòng nhập firstname");
      return false;
    }
    //validate last name
    if (paramStudentObj.lastname === "") {
      alert("Xin vui lòng nhập lastname");
      return false;
    }
    //validate email
    if (paramStudentObj.email === "") {
      alert("Xin vui lòng nhập email");
      return false;
    }
    var vValidEmail = validateEmail(paramStudentObj.email);
    if (!vValidEmail) {
      alert("Xin vui lòng nhập email đúng định dạng (a@gmail.com)");
      return false;
    }
    if (isExistEmail(paramStudentObj.email)) {
      alert("Email của bạn bị trùng");
      return false;
    }
    //validate Study Year
    if (isNaN(paramStudentObj.studyYear) || paramStudentObj.studyYear <= 2000) {
      alert("Xin vui lòng nhập study year");
      return false;
    }
    //validate ngay thang
    var vValidDate = isValidDate(paramStudentObj.birthday);
    if (!vValidDate) {
      alert("Xin vui lòng nhập birthday");
      return false;
    }
    return true;
  }
  //Kiểm tra username có bị trùng không
  function isExistUsername(paramUsername) {
    "use strict";
    var vFound = false;
    var vIndex = 0;
    if (gFormMode === gFORM_MODE_INSERT) {
      //thêm mới => không được phép trùng với cái đã có
      while (!vFound && vIndex < gStudentDB.students.length) {
        if (gStudentDB.students[vIndex].username === paramUsername) {
          vFound = true;
        } else {
          vIndex++;
        }
      }
    }
    return vFound;
  }
  //Kiểm tra studentCode có bị trùng không
  function isExistStudentCode(paramStudentCode) {
    "use strict";
    var vFound = false;
    var vIndex = 0;
    if (gFormMode === gFORM_MODE_INSERT) {
      //thêm mới => không được phép trùng với cái đã có
      while (!vFound && vIndex < gStudentDB.students.length) {
        if (gStudentDB.students[vIndex].studentCode == paramStudentCode) {
          vFound = true;
        } else {
          vIndex++;
        }
      }
    } else {
      //sửa
      while (!vFound && vIndex < gStudentDB.students.length) {
        if (
          gStudentDB.students[vIndex].studentCode == paramStudentCode &&
          gStudentDB.students[vIndex].id !== gStudentId
        ) {
          vFound = true;
        } else {
          vIndex++;
        }
      }
    }
    return vFound;
  }
  //Hàm validate định dạng của email
  function validateEmail(paramEmail) {
    "use strict";
    var vFormat =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (paramEmail === "") {
      return true;
    }
    if (paramEmail !== "") {
      if (!vFormat.test(paramEmail)) {
        return false;
      }
      return true;
    }
  }
  //Kiểm tra email có bị trùng không
  function isExistEmail(paramEmail) {
    "use strict";
    var vFound = false;
    var vIndex = 0;
    if (gFormMode === gFORM_MODE_INSERT) {
      //thêm mới => không được phép trùng với cái đã có
      while (!vFound && vIndex < gStudentDB.students.length) {
        if (gStudentDB.students[vIndex].email == paramEmail) {
          vFound = true;
        } else {
          vIndex++;
        }
      }
    } else {
      //sửa
      while (!vFound && vIndex < gStudentDB.students.length) {
        if (
          gStudentDB.students[vIndex].email == paramEmail &&
          gStudentDB.students[vIndex].id !== gStudentId
        ) {
          vFound = true;
        } else {
          vIndex++;
        }
      }
    }
    return vFound;
  }
  //Hàm validate ngày tháng năm
  function isValidDate(paramDate) {
    "use strict";
    // Assumes paramDate is "dd/mm/yyyy"
    if (!/^\d\d\/\d\d\/\d\d\d\d$/.test(paramDate)) {
      return false;
    }
    var vDateParts = paramDate.split("/").map((p) => parseInt(p, 10));
    vDateParts[1] -= 1;
    var vDate = new Date(vDateParts[2], vDateParts[1], vDateParts[0]);
    return (
      vDate.getDate() === vDateParts[0] &&
      vDate.getMonth() === vDateParts[1] &&
      vDate.getFullYear() === vDateParts[2]
    );
  }

  //Hàm lưu dữ liệu student vào gStudentDB
  function saveStudentData(paramStudent) {
    "use strict";
    if (gFormMode === gFORM_MODE_INSERT) {
      //thêm mới => thêm luôn vào cuối mảng
      gStudentDB.addNewStudent(paramStudent);
    } else {
      //sửa => cập nhật vào đúng vị trí của student trong mảng

      gStudentDB.editStudent(paramStudent);
    }
  }

  //Hàm lấy id cho subject
  function getSubjectNextId() {
    "use strict";
    var vNextId = 0;
    // nếu mảng chưa có phần tử nào, thì id sẽ bắt đầu từ 1
    if (gSubjects.subjects.length == 0) {
      vNextId = 1;
    } else {
      // id tiếp theo bằng id của phần tử cuối cùng cộng thêm 1
      vNextId = gSubjects.subjects[gSubjects.subjects.length - 1].id + 1;
    }
    return vNextId;
  }
  //hàm thhu thập dữ liệu subject
  function getSubjectObjInfo(paramSubject) {
    "use strict";
    if (gFormMode === gFORM_MODE_INSERT) {
      //thêm mới
      paramSubject.id = getSubjectNextId();
    } else {
      //sửa
      paramSubject.id = gSubjectId;
    }

    paramSubject.subjectCode = $("#inp-subjectcode").val();
    paramSubject.subjectName = $("#inp-subjectname").val();
    paramSubject.credit = $("#inp-credit").val();
  }

  //Validate dữ liệu subject
  function validateSubject(paramSubject) {
    "use strict";
    //validate subject code
    if (paramSubject.subjectCode === "") {
      alert("Hãy nhập subject code");
      return false;
    }
    if (isExistSubjectCode(paramSubject.subjectCode)) {
      alert("Subject code bị trùng");
      return false;
    }
    //validate subject name
    if (paramSubject.subjectName === "") {
      alert("Hãy nhập subject name");
      return false;
    }
    if (paramSubject.credit === "") {
      alert("Hãy nhập credit");
      return false;
    }
    if (
      isNaN(paramSubject.credit) ||
      paramSubject.credit <= 0 ||
      paramSubject.credit > 20
    ) {
      alert("Credit không chính xác (0 ~ 20)");
      return false;
    }
    return true;
  }
  //Kiểm tra subject code có bị trùng không
  function isExistSubjectCode(paramSubjectCode) {
    "use strict";
    var vFound = false;
    var vIndex = 0;
    if (gFormMode === gFORM_MODE_INSERT) {
      //thêm mới => không được phép trùng với cái đã có
      while (!vFound && vIndex < gSubjects.subjects.length) {
        if (gSubjects.subjects[vIndex].subjectCode === paramSubjectCode) {
          vFound = true;
        } else {
          vIndex++;
        }
      }
    } else {
      //sửa
      while (!vFound && vIndex < gSubjects.subjects.length) {
        if (
          gSubjects.subjects[vIndex].subjectCode == paramSubjectCode &&
          gSubjects.subjects[vIndex].id !== gSubjectId
        ) {
          vFound = true;
        } else {
          vIndex++;
        }
      }
    }
    return vFound;
  }
  //Hàm save subject vào gsubjectDB

  function saveSubjectData(paramSubject) {
    "use strict";
    if (gFormMode === gFORM_MODE_INSERT) {
      //thêm mới => thêm luôn vào cuối mảng
      gSubjects.addNewSubject(paramSubject);
    } else {
      //sửa => cập nhật vào đúng vị trí của môn học trong mảng

      gSubjects.editSubject(paramSubject);
    }
  }
  //Hàm lấy id cho grade
  function getGradeNextId() {
    "use strict";
    var vNextId = 0;
    // nếu mảng chưa có phần tử nào, thì id sẽ bắt đầu từ 1
    if (gGrades.grades.length == 0) {
      vNextId = 1;
    } else {
      // id tiếp theo bằng id của phần tử cuối cùng cộng thêm 1
      vNextId = gGrades.grades[gGrades.grades.length - 1].id + 1;
    }
    return vNextId;
  }
  //Hàm xử lý thu thập dữ liệu điểm
  function getGradeObjInfo(paramGradeObj) {
    "use strict";
    if (gFormMode === gFORM_MODE_INSERT) {
      paramGradeObj.id = getGradeNextId();
    } else {
      paramGradeObj.id = gId;
    }
    paramGradeObj.studentId = Number($("#select-student-modal").val());
    paramGradeObj.subjectId = Number($("#select-subject-modal").val());
    paramGradeObj.grade = Number($("#inp-diemthi").val());
    paramGradeObj.examDate = $("#inp-ngaythi").val();
  }
  //Hàm validate dữ liệu của grade
  function validateGrade(paramGradeObj) {
    "use strict";
    //validate học sinh
    if (paramGradeObj.studentId === "") {
      alert("Vui lòng chọn học sinh");
      return false;
    }
    //validate môn học
    if (paramGradeObj.subjectId === "") {
      alert("Vui lòng chọn môn học");
      return false;
    }
    //validate điểm
    if (paramGradeObj.grade === "") {
      alert("Vui lòng nhập điểm");
      return false;
    }
    if (
      isNaN(paramGradeObj.grade) ||
      paramGradeObj.grade < 0 ||
      paramGradeObj.grade > 10
    ) {
      alert("Điểm số không hợp lệ");
      return false;
    }
    //validate ngày thi

    var vValidDate = isValidDate(paramGradeObj.examDate);
    if (!vValidDate) {
      alert("Vui lòng nhập ngày thi (dd/mm/yy)");
      return false;
    }
    return true;
  }
  //Hàm save grade data vào gGrades
  function saveGradeData(paramGradeObj) {
    "use strict";
    if (gFormMode === gFORM_MODE_INSERT) {
      //thêm mới => thêm luôn vào cuối mảng
      gGrades.addNewGrade(paramGradeObj);
    } else {
      //sửa => cập nhật vào đúng vị trí của grade trong mảng

      gGrades.editGrade(paramGradeObj);
    }
  }

  //Hàm reset các trường dữ liệu
  function resetForm() {
    "use strict";
    //Tắt read only
    //Thông tin học sinh
    $("#inp-username").prop("readonly", false);

    //Xoá các trường dữ liệu
    $("#select-student-modal").val("");
    $("#select-subject-modal").val("");
    $("#inp-diemthi").val("");
    $("#inp-ngaythi").val("");

    $("#inp-subjectname").val("");
    $("#inp-subjectcode").val("");
    $("#inp-credit").val("");

    $("#inp-username").val("");
    $("#inp-studentcode").val("");
    $("#inp-fname").val("");
    $("#inp-lname").val("");
    $("#inp-studyyear").val("");
    $("#inp-bday").val("");
    $("#inp-email").val("");
    //ẩn các trường div dữ liệu thông tin và hiện ra 3 nút
    $("#div-show-subject").hide();
    $("#div-show-student").hide();
    $("#div-show-grade").hide();
    $("#div-button").show();
    //mở lại nút

    $("#btn-check-username").prop("disabled", false);
  }
  //Disable check button khi edit
  function disableCheckButton() {
    "use strict";

    $("#btn-check-username").prop("disabled", true);
  }
});
